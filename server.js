const port = 5000

const bodyParser = require('body-parser')
const express = require('express')
const server = express()

server.use(bodyParser.urlencoded({ limit: '100mb', extended: true }))
server.use(bodyParser.json({limit: '100mb'}))

server.listen(port, function() {
    console.log(`BACKEND is running on port ${port}.`)
})

module.exports = server