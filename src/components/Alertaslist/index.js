import React, { Component } from 'react';
import { 
    Container, 
    Row, 
    Col,
    Button, 
    ButtonGroup,
  } from "react-bootstrap";

import { BootstrapTable, TableHeaderColumn}  from 'react-bootstrap-table';

export class Alertaslist extends Component {
    constructor(props) {
        super(props);
    }

    mudarStatus(e, a, b){
        let { alertas } = this.props;

        Promise.resolve(
            alertas.forEach(element => {
                console.log(element.funcionario);
                if(Number(element._id) == Number(a)){
                    element.status_alerta = 'ATENDIDO';
                }
            })
        ).then((element)=>{
            this.setState({alertas:{alertas:this.alertas}}, () => {
                let localS = JSON.parse(localStorage.getItem('alertas'));
                
                console.log(localS);
                console.log(localS.alertas);
                console.log(localS.inventario_id);
                localS.alertas = alertas;
                console.log(localS.alertas);
                localStorage.setItem('alertas',JSON.stringify(localS));
            });
        })
        
        
    }

    render(){

        let lista = this.props.alertas;

        return(
            <div>
                <BootstrapTable data={lista.filter( item => item.status_alerta == 'PENDENTE')} height='350' scrollTop={ 'Bottom' } 
                    options={ {
                    defaultSortName: '_id', 
                    noDataText: 'Não há dados para exibir',
                    defaultSortName: '_id',  // default sort column name
                    defaultSortOrder: 'asc'  // default sort order
                } }>
                    <TableHeaderColumn dataField='_id' isKey hidden >ID</TableHeaderColumn>
                    <TableHeaderColumn dataField='funcionario'  width='180' >funcionário</TableHeaderColumn>
                    {/* <TableHeaderColumn dataField='status_alerta'  width='80' >Atendimento</TableHeaderColumn> */}
                    <TableHeaderColumn dataField='' width='90' dataAlign='center'
                    dataFormat={ (cell, row) => {
                        return (
                            <Button variant="success" size="sm" onClick={
                                e => this.mudarStatus(e, row._id, row.status_alerta)
                                }>
                                Atendido
                            </Button>
                        ) 
                    }  }
                >Ações</TableHeaderColumn>
                </BootstrapTable>
            </div>
        );
    }

}

export default Alertaslist;