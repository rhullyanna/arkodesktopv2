import React, { Component } from 'react';
import { Pie } from 'react-chartjs-2';
import { Row, Col } from 'react-bootstrap';
import './styles.css';



export class Cardpizza extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const {data_grafico_pizza} = this.props;

        const enderecamentos = {
            coletados: {
                label:'Coletados',
                quantidade: data_grafico_pizza.concluidos,
                cor: '#28a745',
            },
            nao_coletados: {
                label:'Não Coletados',
                quantidade: data_grafico_pizza.pendentes,
                cor: '#6c757d',
            },
            iniciados: {
                label:'Iniciados',
                quantidade: data_grafico_pizza.iniciados,
                cor: '#ffc107',
            }
        };

        const data = {
            labels: [
                enderecamentos.coletados.label,
                enderecamentos.nao_coletados.label,
                enderecamentos.iniciados.label
            ],
            datasets: [{
                data: [
                    enderecamentos.coletados.quantidade, 
                    enderecamentos.nao_coletados.quantidade, 
                    enderecamentos.iniciados.quantidade
                ],
                backgroundColor: [
                    enderecamentos.coletados.cor,
                    enderecamentos.nao_coletados.cor,
                    enderecamentos.iniciados.cor
                ],
                hoverBackgroundColor: [
                    enderecamentos.coletados.cor,
                    enderecamentos.nao_coletados.cor,
                    enderecamentos.iniciados.cor
                ]
            }]
        };

        return (
            <div>
                <Pie 
                  data={data} 
                  width={100}
                  height={50}
                  legend={false}
                />
                <Row >
                    <Col md={4}>
                        <div className='item-cardpizza'>
                            <h1>
                                <label style={{'margin-right':'5px' ,'width':'10px', 'height':'10px', 'border-radius':'50%', 'background':enderecamentos.nao_coletados.cor}}></label>
                                End. Pendentes
                            </h1>
                            <p>{enderecamentos.nao_coletados.quantidade}</p>
                        </div>
                    </Col>
                    <Col md={4}>
                        <div className='item-cardpizza'>
                            <h1>
                                <label style={{'margin-right':'5px' ,'width':'10px', 'height':'10px', 'border-radius':'50%', 'background':enderecamentos.iniciados.cor}}></label>
                                End. iniciados
                            </h1>
                            <p>{enderecamentos.iniciados.quantidade}</p>
                        </div>
                    </Col>
                    <Col md={4}>
                        <div className='item-cardpizza'>
                            <h1>
                                <label style={{'margin-right':'5px' ,'width':'10px', 'height':'10px', 'border-radius':'50%', 'background':enderecamentos.coletados.cor}}></label>
                                End. Concluídos
                            </h1>
                            <p>{enderecamentos.coletados.quantidade}</p>
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }
}

export default Cardpizza;

