import React, { Component } from "react";
import { 
  Navbar,
  Nav,
  NavDropdown, 
  Form,
  FormControl,
  Button,
  NavItem
} from "react-bootstrap";

import logo from "../../assets/img/logo.png";
import { ipcRenderer } from "electron";

class InventNavbar extends Component {
sair(){
  localStorage.clear();
  ipcRenderer.send('executarSincronizacao');
}
  render() {
    return (
      <Navbar collapseOnSelect bg="dark" variant="dark" expand="md">
        <Navbar.Brand>
          <img
            src={logo}
            height="30"
            className="d-inline-block align-top"
            alt="React Bootstrap logo"
          /></Navbar.Brand>
        <Navbar.Collapse className="justify-content-end">
          {/* <Button variant="outline-info" href="#/auth/login" onClick={()=>localStorage.clear()}>Sair</Button> */}
          <Button variant="outline-info" href="#/auth/login" onClick={()=>{this.sair()}}>Sair</Button>
        </Navbar.Collapse>
      </Navbar>
    );
  }
}

export default InventNavbar;
