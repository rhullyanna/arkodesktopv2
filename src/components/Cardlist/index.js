import React, { Component } from 'react';
import { 
    Container, 
    Row, 
    Col,
    Button, 
    ButtonGroup,
    Modal
  } from "react-bootstrap";

  import { BootstrapTable, TableHeaderColumn}  from 'react-bootstrap-table'

export class Cardlist extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showModalCod: false,
            enderecamentoCod: []
        }
    }

    handleShow(ev, end_desc) {
        Promise.resolve( 
          this.props.coleta.filter( i =>
            i.enderecamento === end_desc
          )
        ).then( enderecamentoCod =>{
            this.setState({ enderecamentoCod, showModalCod: true }, ()=>{
                console.log(this.state);
            })
        })
    }

    handleClose(ev) {
        Promise.resolve( 
            console.log('fechar modal')
        ).then(() => {
            this.setState({ enderecamentoCod:[], showModalCod: false }, ()=>{
                console.log(this.state);
            })
        })
    }

    render() {

        const lista = this.props.lista;
        const { enderecamentoCod, showModalCod} = this.state;

        const options = {
            defaultSortName: 'valor_divergente', 
            noDataText: 'Não há dados para exibir',
            exportCSVText: 'Exportar para csv'
          }

        return (
            <div>
                <Row >
                    <Col style={{
                        // 'marginBottom': '10px',
                    }}>
                        <BootstrapTable data={lista.filter( item => item.status == 'CONCLUIDO')} height='300' scrollTop={ 'Bottom' } 
                        // <BootstrapTable data={lista.filter( item => item.status == 'concluido')} height='230' scrollTop={ 'Bottom' } 
                            options={ {
                            defaultSortName: 'descricao', 
                            noDataText: 'Não há dados para exibir',
                            defaultSortName: 'descricao',  // default sort column name
                            defaultSortOrder: 'desc'  // default sort order
                        } }>
                            <TableHeaderColumn dataField='_id' isKey hidden>ID</TableHeaderColumn>
                            <TableHeaderColumn dataField='descricao' hidden width='80' >Código</TableHeaderColumn>
                            <TableHeaderColumn dataField='' width='90' dataAlign='center'
							dataFormat={ (cell, row) => {
								return (
									<Button variant="success" size="sm" 
                                        onClick={e => this.handleShow(e, row.descricao)
                                    }>
										{row.descricao}
									</Button>
								) 
							}  }
						>Coletado</TableHeaderColumn>
                        </BootstrapTable>
                        
                    </Col>
                    <Col style={{
                        'marginBottom': '10px',
                    }}>
                        <BootstrapTable data={lista.filter( item => item.status == 'INICIADO')} height='300' scrollTop={ 'Bottom' } 
                            options={ {
                            defaultSortName: 'descricao', 
                            noDataText: 'Não há dados para exibir',
                            defaultSortName: 'descricao',  // default sort column name
                            defaultSortOrder: 'desc'  // default sort order
                        } }>
                            <TableHeaderColumn dataField='_id' isKey hidden>ID</TableHeaderColumn>
                            <TableHeaderColumn dataField='descricao' hidden width='80' >Código</TableHeaderColumn>
                            <TableHeaderColumn dataField='' width='90' dataAlign='center'
							dataFormat={ (cell, row) => {
								return (
									<Button variant="warning" size="sm" >
										{row.descricao}
									</Button>
								) 
							}  }
						>Coletando</TableHeaderColumn>
                        </BootstrapTable>
                        
                    </Col>
                    <Col style={{
                        'marginBottom': '10px',
                    }}>
                        <BootstrapTable data={lista.filter( item => item.status == 'ERRO')} height='300' scrollTop={ 'Bottom' } 
                            options={ {
                            defaultSortName: 'descricao', 
                            noDataText: 'Não há dados para exibir',
                            defaultSortName: 'descricao',  // default sort column name
                            defaultSortOrder: 'desc'  // default sort order
                        } }>
                            <TableHeaderColumn dataField='_id' isKey hidden>ID</TableHeaderColumn>
                            <TableHeaderColumn dataField='descricao' hidden width='80' >Código</TableHeaderColumn>
                            <TableHeaderColumn dataField='' width='90' dataAlign='center'
							dataFormat={ (cell, row) => {
								return (
									<Button variant="danger" size="sm" >
										{row.descricao}
									</Button>
								) 
							}  }
						>Erro Valid.</TableHeaderColumn>
                        </BootstrapTable>
                        
                    </Col>
                    <Col style={{
                        'marginBottom': '10px',
                    }}>
                        <BootstrapTable data={lista.filter( item => item.status == 'PENDENTE')} height='300' scrollTop={ 'Bottom' } 
                            options={ {
                            defaultSortName: 'descricao', 
                            noDataText: 'Não há dados para exibir',
                            defaultSortName: 'descricao',  // default sort column name
                            defaultSortOrder: 'desc'  // default sort order
                        } }>
                            <TableHeaderColumn dataField='_id' isKey hidden>ID</TableHeaderColumn>
                            <TableHeaderColumn dataField='descricao' hidden width='80' >Código</TableHeaderColumn>
                            <TableHeaderColumn dataField='' width='90' dataAlign='center'
							dataFormat={ (cell, row) => {
								return (
									<Button variant="secondary" size="sm" >
										{row.descricao}
									</Button>
								) 
							}  }
						>Não Coletado</TableHeaderColumn>
                        </BootstrapTable>
                        
                    </Col>
                </Row>
                <Modal show={showModalCod} onHide={e => this.handleClose()}>
                    <Modal.Header closeButton>
                    <Modal.Title>Códigos Inventariados</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                    <BootstrapTable data={enderecamentoCod} height='350' scrollTop={ 'Bottom' } 
                        search exportCSV options={ options }>
                        <TableHeaderColumn dataField='cod_barra' isKey >EAN COLETA ({enderecamentoCod.length})</TableHeaderColumn>
                        <TableHeaderColumn dataField='qtd' >QUANT</TableHeaderColumn>
                    </BootstrapTable>
                    </Modal.Body>
                    <Modal.Footer>
                    <Button variant="secondary" onClick={e => this.handleClose()}>
                        Fechar
                    </Button>
                    </Modal.Footer>
                </Modal>
            </div>
        )
    }
}

export default Cardlist;