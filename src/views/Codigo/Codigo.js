import React, { Component } from "react"
import mysql from 'mysql'
import env from '../../../.env'
import nedb from 'nedb'
import { 
  Container,
  Row, 
  Col, 
  Button,
  Form
} from "react-bootstrap";
import readXlsxFile from 'read-excel-file/node';

const { ipcRenderer } = window.require('electron')

const base_db = new nedb({filename: 'data/base.json'})
const coleta_db = new nedb({filename: 'data/coleta.json'})

export default class Codigo extends Component {
	constructor(props){
    super(props);
    this.state = {
      base: [],
      fora_base: [],
      upload_fora_base: [],
      coleta: [],
      inventario_id: localStorage.getItem('inv_id') || '',
      isDivergenciaPorGrupo: localStorage.getItem('isDivergenciaPorGrupo'),
      file: null
    }
  }
  componentDidMount() { 
    const { inventario_id,isDivergenciaPorGrupo,base } = this.state
    
    console.log("isDivPorGrupo: "+this.state.isDivergenciaPorGrupo);
    console.log(this.state);
    this.getBase().then(()=>{
      if(isDivergenciaPorGrupo){
        this.getColetaPorCodigoInterno()
      }else{
        this.getColeta()
      }
    })
    
    // this.getColeta()

    // base_db.find({inventario_id: Number(inventario_id)}, {cod_barra: 1, _id:0},(err,docs)=>{
    //   if (err) {
    //     alert(`Erro ao ler a base, código: ${err}`)
    //   } else{
    //     console.log(docs)
    //   }
    // })
    // coleta_db.find({inventario_id: Number(inventario_id)}, {cod_barra: 1, enderecamento: 1, itens_embalagem: 1, _id: 0},(err,doc)=>console.table(doc))
  }

  onFormSubmitPorGrupo(e){
    e.preventDefault() // Stop form submit
    const {inventario_id, file, fora_base} = this.state;
    let fora_base_sobra = [];
    if(file !== null) {
      readXlsxFile(file.path).then((rows) => {
        let upload_fora_base = rows.slice(1).map( element =>
          ({
                'cod_barras': element[0],
                'cod_interno': element[1],
                'departamento': element[2],
                'setor_secao': element[3],
                'descricao_setor_secao': element[4],
                'grupo': element[5],
                'familia': element[6],
                'subfamilia': element[7],
                'referencia': element[8],
                'saldo_qtd_estoque': element[9],
                'valor_custo': element[10],
                'valor_venda': element[11],
                'descricao_item': element[12],
                'inventario': Number(inventario_id)
              })
        );

        // Promise.resolve(
          
        //   upload_fora_base.forEach(item_upload => {
        //     let upload_esta_nos_fora_base = false;
        //     Promise.resolve(
        //       fora_base.forEach(item_fora_base => {
        //         if(item_fora_base.cod_barra.toString() == item_upload.cod_barras.toString()){
        //           upload_esta_nos_fora_base = true;
        //           console.log(item_upload);
        //         }
        //       })
        //     ).then(()=>{
        //       if(upload_esta_nos_fora_base != true){
        //         fora_base_sobra.push(item_upload);
        //       }
        //     })

        //   })
        // )


        Promise.resolve(
          
          fora_base.forEach(item_fora_base => {
            let upload_esta_nos_fora_base = true;
            Promise.resolve(
              upload_fora_base.forEach(item_upload => {
                if(item_fora_base.cod_barra.toString() == item_upload.cod_barras.toString()){
                  upload_esta_nos_fora_base = false;
                  console.log(item_upload);
                }
              })
            ).then(()=>{
              if(upload_esta_nos_fora_base == true){
                fora_base_sobra.push(item_fora_base);
              }
            })
          })
        ).then(()=>{
          this.setState({upload_fora_base,fora_base:fora_base_sobra}, () => {
            console.log(this.state.upload_fora_base);
            console.log("fora_base_sobra")
            console.log(fora_base_sobra)
            this.removeSobraDosForaDaBaseDaColeta();
          })
        })
      })
    }
  }

  removeSobraDosForaDaBaseDaColeta(){
    console.log("removeSobraDosForaDaBaseDaColeta")
    const {fora_base} = this.state;
    let coleta = ipcRenderer.sendSync('getColeta', 'coleta');
    let nova_coleta = [];
    console.log("coleta")
    console.log(coleta)
    Promise.resolve(
      coleta.forEach(item_coleta => {
        let item_esta_fora_da_base = false;
        Promise.resolve(
          fora_base.forEach(item_fora_base => {
            if(item_fora_base.cod_barra.toString() == item_coleta.cod_barra.toString()){
              item_esta_fora_da_base = true;
            }
          })
        ).then(()=>{
          if(item_esta_fora_da_base == false){
            nova_coleta.push(item_coleta)
          }
          
        })
      })
    ).then(()=>{
      console.log("nova_coleta");
      console.log(nova_coleta);
    })
  }

  onFormSubmit(e){
    e.preventDefault() // Stop form submit
    const {inventario_id, file} = this.state;
    if(file !== null) {
      console.log("file != null");
      console.log(this.state.file.path);

      readXlsxFile(file.path).then((rows) => {
        console.log(rows);
        let upload_fora_base = rows.slice(1).map( element =>
          ({
                'cod_barras': element[0],
                'cod_interno': element[1],
                'departamento': element[2],
                'setor_secao': element[3],
                'descricao_setor_secao': element[4],
                'grupo': element[5],
                'familia': element[6],
                'subfamilia': element[7],
                'referencia': element[8],
                'saldo_qtd_estoque': element[9],
                'valor_custo': element[10],
                'valor_venda': element[11],
                'descricao_item': element[12],
                'inventario': Number(inventario_id)
              })
        )
        // this.setState({base}, () => {
        this.setState({upload_fora_base}, () => {
          // console.log(this.state.base);
          console.log("this.state");
          console.log(this.state);
        })
      })
    }
  }
  onChange(e) {
    console.log('passou no onchange(e)');
    this.setState({file:e.target.files[0]},()=>{
      console.log("state file: ");
      console.log(this.state.file);
    })

  }

  getBase(){
    return new Promise((resolve, reject)=>{
      const base = ipcRenderer.sendSync('getBase', 'base');

      const {upload_fora_base} = this.state;

      console.log('upload_fora_base');
      console.log(upload_fora_base);
      Promise.resolve(
        upload_fora_base.map(item => {
          console.log(item);
          base.push(item);
        })
      ).then(() => {
        resolve();
      })

      console.log("base antes da promise");
      console.log(base);
      Promise.resolve(
        base.map(b=>{
          b['cod_barra']             = b['cod_barras']
          b['cod_interno']           = b['cod_interno']
          b['descricao_setor_secao'] = b['departamento']
          b['inventario_id']         = b['inventario']
          b['saldo_estoque']         = b['saldo_qtd_estoque']
          b['id']                    = b['_id']
          return b
        })
      ).then(base=>{
        this.setState({base},()=>{
            console.log("base carregada no state");
            console.log(this.state.base);
        })
        resolve()
      })
    })
  }

  getColetaPorCodigoInterno(){

      // console.log(ipcRenderer.sendSync('synchronous-message', 'ping')) // prints "pong"
      console.log("getColetaPorCodigoInterno");
      let {inventario_id,base,fora_base} = this.state
      const coleta = ipcRenderer.sendSync('getColeta', 'coleta')
      let results = []
      let array_com_cod_e_qtd = [];

      new Promise((resolve,reject)=>{
          fora_base = coleta.filter(c => !base.map(b=>b.cod_barras.toString()).includes(c.cod_barra.toString()))
          this.setState({fora_base:fora_base},()=>{
          })
          resolve()
      }).then(()=>{
        Promise.resolve(
          fora_base.forEach(element => {
            let contador = 0;
            Promise.resolve(
              fora_base.forEach(segLoop => {
                if(results.indexOf(element.cod_barra) < 0){
                  console.log('indexOF')
                  console.log(results.indexOf(element.cod_barra))
                  results.push(element.cod_barra);
                  array_com_cod_e_qtd.push({
                    'cod_barra' : element.cod_barra, 
                    'cod_interno': element.cod_interno,
                    'enderecamento': element.enderecamento,
                    'qtd_inventario':contador,
                    'validade': element.validade,
                    'lote': element.lote,
                    'fabricacao': element.fabricacao,
                    'inventario_id': element.inventario_id
                  })
                }                
              })
            ).then(()=>{
              Promise.resolve(
                array_com_cod_e_qtd.forEach(adicionado => {
                  if(adicionado.cod_barra.toString() == element.cod_barra.toString()){
                    adicionado.qtd_inventario += 1
                  }
                })
              )
            })
          })
        ).then(()=>{
          this.setState({fora_base:array_com_cod_e_qtd})
        })
      })
  }

  getColeta() {
    // console.log(ipcRenderer.sendSync('synchronous-message', 'ping')) // prints "pong"
    const {inventario_id} = this.state
    let connection = mysql.createConnection(env.config_mysql);
    let query = `
    SELECT c.* 
    FROM (
      select cod_barra, enderecamento, SUM(itens_embalagem) as 'qtd' 
      from coleta 
      where inventario_id = ? and tipo_coleta="INVENTARIO" 
      GROUP BY cod_barra, enderecamento 
      ) c
    LEFT OUTER JOIN (select cod_barra from base where inventario_id = ?) b
    ON  c.cod_barra = b.cod_barra
    where b.cod_barra IS NULL ORDER BY qtd DESC`
    connection.query(query, [inventario_id,inventario_id],(error, coleta, fields) => {
      if(error){
          console.log(error.code,error.fatal)
          return
      }
      this.setState({coleta})
      connection.end();
    })
  }
  handleClick() {
    const { base, upload_fora_base } = this.state
    if (upload_fora_base.length) {
      alert('Base enviada para o processo principal')
      if(window.localStorage.getItem('upload_fora_base')){
        window.localStorage.setItem('upload_fora_base', '');
      }
      window.localStorage.setItem('upload_fora_base', JSON.stringify(upload_fora_base));
    } else {
      alert('Base vazia')
    }
    alert('Ir para divergência')
    if(this.state.isDivergenciaPorGrupo == 'true'){
      this.props.history.push('/admin/divergenciaporgrupo')
    }else{
      this.props.history.push('/admin/divergencia')
    }
  }
  render() {
    
    const options = {
      defaultSortName: 'valor_divergente', 
      noDataText: 'Não há dados para exibir',
      exportCSVText: 'Exportar para csv'
    }
    const { coleta, upload_fora_base,fora_base } = this.state
    console.log("isDivPorGrupo: "+this.state.isDivergenciaPorGrupo);

    if(this.state.isDivergenciaPorGrupo == 'true'){
      return (
        <div className="content">
          <h1>Códigos Novos - Processo por grupo</h1>
          <Container fluid>
          <Row>
            <Col>
              <Form onSubmit={this.onFormSubmitPorGrupo.bind(this)}>
                <Form.Label>Carregar arquivo</Form.Label>
                <Form.Control size="sm" type="file" onChange={this.onChange.bind(this)} />
                <div className="p-2"/>
                <Button  variant="info" type="submit">Carregar</Button>
                <div className="p-2"/>
              </Form>
            </Col>
          </Row>
          <Row>
            <Col md={6} style={{ 'marginBottom': '30px' }}>
              {/* <BootstrapTable data={coleta} height='250' scrollTop={ 'Bottom' }  */}
              <BootstrapTable data={fora_base} height='250' scrollTop={ 'Bottom' } 
                search exportCSV options={ options }>
                <TableHeaderColumn dataField='cod_barra' isKey>EAN COLETA ({coleta.length})</TableHeaderColumn>
                <TableHeaderColumn dataField='enderecamento'>ENDERECAMENTO</TableHeaderColumn>
                <TableHeaderColumn dataField='qtd_inventario'>QUANT</TableHeaderColumn>
              </BootstrapTable>
            </Col>
            <Col md={6} style={{ 'marginBottom': '30px' }}>
              <BootstrapTable data={upload_fora_base} height='250' scrollTop={ 'Bottom' } 
                search options={ options }>
                <TableHeaderColumn dataField='cod_barras' width='140' isKey >Cod. Interno BASE ({upload_fora_base.length})</TableHeaderColumn>
                <TableHeaderColumn dataField='descricao_item' tdStyle={{whiteSpace: 'normal'}}>DESCRIÇÃO</TableHeaderColumn>
                <TableHeaderColumn dataField='saldo_qtd_estoque' width='70'>SALDO</TableHeaderColumn>
              </BootstrapTable>
            </Col>
          </Row>
          <div>
            <Button variant="info" onClick={this.handleClick.bind(this)}>Salvar</Button>
            <div className="p-2"/>
          </div>
        </Container>
        </div>
      );
    }else{
      return (
        <div className="content">
          <h1>Códigos Novos</h1>
          <Container fluid>
          <Row>
            <Col>
              <Form onSubmit={this.onFormSubmit.bind(this)}>
                <Form.Label>Carregar arquivo</Form.Label>
                <Form.Control size="sm" type="file" onChange={this.onChange.bind(this)} />
                <div className="p-2"/>
                <Button  variant="info" type="submit">Carregar</Button>
                <div className="p-2"/>
              </Form>
            </Col>
          </Row>
          <Row>
            <Col md={6} style={{ 'marginBottom': '30px' }}>
              <BootstrapTable data={coleta} height='250' scrollTop={ 'Bottom' } 
                search exportCSV options={ options }>
                <TableHeaderColumn dataField='cod_barra' isKey>EAN COLETA ({coleta.length})</TableHeaderColumn>
                <TableHeaderColumn dataField='enderecamento'>ENDERECAMENTO</TableHeaderColumn>
                <TableHeaderColumn dataField='qtd'>QUANT</TableHeaderColumn>
              </BootstrapTable>
            </Col>
            <Col md={6} style={{ 'marginBottom': '30px' }}>
              <BootstrapTable data={upload_fora_base} height='250' scrollTop={ 'Bottom' } 
                search options={ options }>
                <TableHeaderColumn dataField='cod_barras' width='140' isKey >EAN BASE ({upload_fora_base.length})</TableHeaderColumn>
                <TableHeaderColumn dataField='descricao_item' tdStyle={{whiteSpace: 'normal'}}>DESCRIÇÃO</TableHeaderColumn>
                <TableHeaderColumn dataField='saldo_qtd_estoque' width='70'>SALDO</TableHeaderColumn>
              </BootstrapTable>
            </Col>
          </Row>
          <div>
            <Button variant="info" onClick={this.handleClick.bind(this)}>Salvar</Button>
            <div className="p-2"/>
          </div>
        </Container>
        </div>
      );
    }

    
  }
}