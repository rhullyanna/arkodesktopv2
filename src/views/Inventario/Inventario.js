import React, { Component } from "react";
const { ipcRenderer } = window.require('electron')
import nedb from 'nedb'
var inventario_db = new nedb({filename: 'data/inventario.json', autoload: true})
var agendamento_db = new nedb({filename: 'data/agendamento.json', autoload: true})
var loja_db = new nedb({filename: 'data/loja.json', autoload: true})

import { BootstrapTable, TableHeaderColumn}  from 'react-bootstrap-table'
import InventNavbar from "../../components/Navbars/InventNavBar"
import {
	Container,
	Table,
	Tooltip,
	Button,
	Row, 
  	Col
} from "react-bootstrap"

function btnFormatter(cell, row) {
	return `<Button class='btn btn-info' size='sm' onClick=this.abrirInventario(this, '${row.id}', '${row.tipo_inventario}', '${row.inventario_status}')>
	  Inventário
  </Button>`;
// return `<p><span class='glyphicons ${cell.icon}' aria-hidden='true'></span> ${cell.pname}, from ${cell.year}</p>`;
}

class Inventario extends Component {
	constructor(props){
    super(props);
    this.state = {
    	usuario_coordenador_id: localStorage.getItem('user_id') || '',
    	inventarios: []
    }
    if(props.location.state){
    	this.state = {
    		usuario_coordenador_id: props.location.state['id'],
    		inventarios: []
    	}
    }
	this.abrirInventario = this.abrirInventario.bind(this)
  }

  componentDidMount() {
		const { usuario_coordenador_id } = this.state
		let inventarios_a_mostrar = [];
		if (usuario_coordenador_id) {
				var results = [];
				new Promise(function( resolve, reject ){
					inventario_db.find(
					{
						usuario_coordenador_id : parseInt(usuario_coordenador_id), 
						inventario_status : { $in : ['ATIVO','EM ANDAMENTO','EM DIVERGENCIA'] }
					},function(err, lista_inventario){	

						if(lista_inventario){
							resolve(lista_inventario);
						}
						else reject('inventario nao encontrado');
				})
				}).then(lista_inventario => {
					// ignora os inventários que não tem agendamento pendente
					lista_inventario.forEach(invent => {
						new Promise(function( resolve, reject ){
								agendamento_db.findOne(
									{
										id: parseInt(invent.agendamento_id),
										agendamento_status : { $in : ['ATIVO','PRE AGENDADO','AGUARDANDO','EM ANDAMENTO','EM DIVERGENCIA'] }
									}, function(err, agend){
									if(agend){
										invent.data = agend.data_agendamento;
										invent.hora = agend.hora_agendamento;
										invent.status = agend.agendamento_status;
										
										inventarios_a_mostrar.push(invent);
										console.log(invent.numero_inventario.split('-')[0].split('L')[1])
										// new Promise(function( resolve, reject ){
										// 	loja_db.findOne(
										// 		{id:parseInt(invent.numero_inventario.split('-')[0].split('L')[1])}
										// 	), function(err, loja){
										// 		if(loja){
										// 			Object.assign(invent,{loja:loja.nome_fantasia});
										// 			resolve(invent)
										// 		}else{
										// 			reject('loja não encontrada');
										// 		}
										// 	}
										// })
										resolve(inventarios_a_mostrar)
									} else {
										reject('agendamento nao encontrado ')
									}
								}.bind(invent))

						}).then(() => {
							// console.log(inventarios_a_mostrar)
							this.setState({inventarios:inventarios_a_mostrar})
						})
					});
				})
					
			}
  }

  

  abrirInventario(e,inv_id, inv_tipo, inv_status){
	
    localStorage.setItem('inv_id', inv_id)
    localStorage.setItem('inv_tipo', inv_tipo)
	localStorage.setItem('inv_status', inv_status)
	ipcRenderer.send('set-inventario', inv_id)
    this.props.history.push('/admin/dashboard')
  }

  

  render() {
	  	const { inventarios} = this.state
    return (
      <div className="content">
        <InventNavbar />
        <h1>Inventarios</h1>
        	<Container fluid>

				<Row>
					<Col md={12} style={{
					'marginBottom': '30px'
					}}>
						<BootstrapTable pagination data={inventarios} height='400px' scrollTop={ 'Bottom' } 
							search options={ {
							defaultSortName: 'tipo_inventario', 
							noDataText: 'Não há dados para exibir',
							defaultSortName: 'id',  // default sort column name
							defaultSortOrder: 'desc'  // default sort order
						} }>
						{/* <TableHeaderColumn dataField='id' isKey hidden>ID</TableHeaderColumn> */}
						<TableHeaderColumn dataField='id' width='10' isKey >ID</TableHeaderColumn>
						<TableHeaderColumn dataField='numero_inventario' width='70' dataSort >Código ({inventarios.length})</TableHeaderColumn>
						<TableHeaderColumn dataField='tipo_inventario' width='70' dataSort >TIPO </TableHeaderColumn>
						<TableHeaderColumn dataField='data' width='70'  >DATA</TableHeaderColumn>
						<TableHeaderColumn dataField='hora' width='70' dataSort>HORA</TableHeaderColumn>
						<TableHeaderColumn dataField='status' width='70' dataSort>STATUS</TableHeaderColumn>
						<TableHeaderColumn dataField='' width='70' 
							dataFormat={ (cell, row) => {
								return (
									<Button variant="info" size="sm" onClick={e => this.abrirInventario(e, row.id, row.tipo_inventario, row.inventario_status)}>
										Inventário
									</Button>
								) 
							}  }
						>Ação</TableHeaderColumn>
						</BootstrapTable>
					</Col>							
				</Row>
        	</Container>
      </div>
    );
  }
}

export default Inventario;
