import React, { Component } from "react";
import { 
  Container, 
  Row, 
  Col,
  Button, 
  ProgressBar,
  ButtonGroup,
  Modal
} from "react-bootstrap";
import {Pie} from 'react-chartjs-2';

import { Card } from "../../components/Card/Card";
import { Cardlist } from "../../components/Cardlist/index";
import { Cardpizza } from "../../components/Cardpizza/index";
import { Alertaslist } from "../../components/Alertaslist/index";
import './styles.css';

import { BootstrapTable, TableHeaderColumn}  from 'react-bootstrap-table'

const { ipcRenderer } = window.require('electron')

class Dashboard extends Component {
constructor(props) {
  super(props);
  this.state = {
    inventario_id: localStorage.getItem('inv_id') || '',
    inventario_status: localStorage.getItem('inv_status') || '',
    tipo_coleta: 'INVENTARIO',
    inventario: [],
    upload_fora_base: [],
    base: [],
    coleta: [],
    isDivergenciaPorGrupo: false, //false
    enderecamento: [],
    equipe: [],
    isPaused: true,
    horas: 0, minutos: 0, segundos: 0,    
    isEnable: false, 
    timeFormat: '00:00:00',
    items: [], text: '',
    progressTotal: 0,
    alertas: {inventario_id:localStorage.getItem('inv_id') || '',alertas:[]},
    validacao:'ATIVA',
    data_grafico_pizza: { iniciados:0, concluidos:0, pendentes:0}
  };

  this.playClock = this.playClock.bind(this);
  this.pauseClock = this.pauseClock.bind(this);
  this.stopClock = this.stopClock.bind(this);    
}
tick() {
  let s = this.state.segundos + 1; 
  let min = this.state.minutos;
  let h = this.state.horas; 
  if (s === 60){
    min = min + 1;
    s = 0;
  }
  if (min === 60){
    h = h + 1;
    min = 0;
  }
  this.setState({
    horas: h, minutos: min, segundos: s
  });
  let timeFormat = this.timeFormater(this.state.horas) + ':' +this.timeFormater(this.state.minutos) +':'+this.timeFormater(this.state.segundos);
  this.setState({timeFormat});
  
}

verificaPauseGuardado(){
  let {horas, minutos, segundos, timeFormat, inventario_id} = this.state;

  if(localStorage.getItem("cronometro")){
    if(Number(JSON.parse(localStorage.getItem("cronometro")).id) == Number(inventario_id)){
      horas = JSON.parse(localStorage.getItem("cronometro")).hora || 0; 
      minutos = JSON.parse(localStorage.getItem("cronometro")).minuto || 0;
      segundos = JSON.parse(localStorage.getItem("cronometro")).segundo || 0;
    }else{
      horas = 0; 
      minutos = 0;
      segundos = 0;
    }
    

    this.setState({horas:horas,minutos:minutos,segundos:segundos}, () => {
      timeFormat = this.timeFormater(this.state.horas) + ':' +this.timeFormater(this.state.minutos) +':'+this.timeFormater(this.state.segundos);
      this.setState({timeFormat});
    })
  }
}

startClock(){
  var futuro = new Date();
  console.log('dia: '+futuro.getDate()+"/"+(futuro.getMonth()+1)+"/"+futuro.getFullYear());
  console.log(futuro.getFullYear()+"-"+(futuro.getMonth()+1)+"-"+futuro.getDate()+" "+futuro.getHours()+":"+futuro.getMinutes()+":"+futuro.getSeconds());
  console.log("milissegundos: "+futuro.getTime())
  console.log("milissegundos -> data: "+new Date(futuro.getTime()).toString());

  this.interval = setInterval(
    () => {
      if(!this.state.isPaused){
        this.tick()
      }
    }, 1000
  ); 
}
playClock(){
  const {tipo_coleta} = this.state
  ipcRenderer.send('asynchronous-message', {controle:'play', tipo_coleta:tipo_coleta})
  this.setState({"isPaused": false, "isEnable": true});

}
stopClock(){
  // alert('Gerar divergencia.');
  let isPorGrupo = false;
  // comentario impedindo que apareça a pergunta para ir pro modulo de divergencia por grupo
  if( confirm('Gerar divergencia por grupo? ') ){
    isPorGrupo = true;
  }

  this.pauseClock();
  this.setState({
    // horas: 0, minutos: 0, segundos: 0, 
    "isEnable": false,
    timeFormat: '00:00:00', 
    "isDivergenciaPorGrupo": isPorGrupo
  }, () => {
    console.log(this.state);
    localStorage.setItem('isDivergenciaPorGrupo', isPorGrupo)
    this.props.history.push('/admin/codigo')
  });
}
pauseClock(){
  const {tipo_coleta, horas, minutos, segundos, inventario_id} = this.state
  let cronometro = {hora: horas, minuto: minutos, segundo: segundos, id:inventario_id};
  ipcRenderer.send('asynchronous-message', {controle:'pause', tipo_coleta})
  this.setState({"isPaused": true, "isEnable": false},()=>{
    localStorage.setItem("cronometro",JSON.stringify(cronometro));
  });
}
timeFormater(time) {
  if (time < 10) {
      time = '0' + time
  }
  return time
}
componentDidMount() {
  this.startClock();
  this.tick();
  this.verificaPauseGuardado();
  this.lerBase();
  this.lerColeta();
  this.lerEnd();
  this.lerEquipe();
  this.atualizacaoEnderecamento();
  this.observandoAlertas();
}

// adiciona os alertas de funcionadios, salvando ele localStorage para manter, caso o usuario abra novamente o msm inventario.
observandoAlertas(){
  const {inventario_id, alertas} = this.state;
  let alertas_aux = JSON.parse(localStorage.getItem('alertas'));
  if(alertas_aux){
    console.log(alertas_aux);
  }
  

  if(alertas_aux){
    if(Number(alertas_aux.inventario_id) == Number(inventario_id)){
      alertas.alertas = alertas_aux.alertas;
      this.setState({alertas:{alertas:alertas_aux.alertas}});
    }else{
      console.log('numero de inventario diferente: '+alertas_aux.inventario_id+' - '+inventario_id);
      this.setState({alertas:{alertas:alertas.alertas}}, () => {
        alertas_aux.inventario_id = inventario_id;
        alertas_aux.alertas = alertas.alertas;
        localStorage.setItem('alertas',JSON.stringify(alertas));
      });
    }
  }else{
    alertas_aux = alertas;
  }
  
  ipcRenderer.on('novoalerta', (event, alerta) => {
    console.log('alertas_aux');
    console.log(alertas_aux);
    console.log('alertas');
    console.log(alertas);
    console.log('recebeu novo alerta');
    alerta._id = alertas_aux.alertas.length+1;
    alertas.alertas = alertas_aux.alertas;
    alertas_aux.alertas.push(alerta);
    this.setState({alertas:{inventario_id:inventario_id,alertas:alertas_aux.alertas}},()=>{
      localStorage.setItem('alertas',JSON.stringify(alertas_aux));
      console.log('adicionado o alerta novo');
    })
  });  
}
atualizacaoEnderecamento(){
  ipcRenderer.on('atualizacaoEnderecamento', (event) => {
    console.log('atualização pela mudança de status');
    // this.lerBase();
    this.lerColeta();
    this.lerEnd();
    this.lerEquipe();
  });
}
componentWillUnmount() {
  clearInterval(this.interval);
}

atualizarGraficoPizza(){
  const {enderecamento} = this.state;

  let iniciados = enderecamento.filter(item => item.status == 'INICIADO');
  let concluidos = enderecamento.filter(item => item.status == 'CONCLUIDO');
  let pendentes = enderecamento.filter(item => item.status == 'PENDENTE');

  let data_grafico_pizza = {
    iniciados:iniciados.length    || 0,
    concluidos:concluidos.length  || 0,
    pendentes:pendentes.length    || 0
  };

  console.log('iniciados: '+iniciados.length);
  console.log('concluidos: '+concluidos.length);
  console.log('pendentes: '+pendentes.length);
  console.log(enderecamento);
  this.setState({data_grafico_pizza:data_grafico_pizza});

}
lerBase(){
  const base = ipcRenderer.sendSync('getBase', 'base')
  this.setState({base})
}
lerColeta(){
    const coleta = ipcRenderer.sendSync('getColeta', 'coleta')
    let results = []
    new Promise((resolve, reject)=>{
      coleta.forEach(col => {
        if (!results.find( elem => {
          if(elem.cod_barra === col.cod_barra && elem.enderecamento === col.enderecamento){
            elem.qtd += col.itens_embalagem
            return true;
          }
          return false;
        })){
          results.push({
            'cod_barra' : col.cod_barra, 
            'enderecamento': col.enderecamento,
            'qtd':col.itens_embalagem,
            'id': col.cod_barra+col.enderecamento
          });
        }
      })
      resolve()
    }).then(()=>{
      console.log("results")
      console.log(results)
      this.setState({coleta: results})
    })
}
lerEnd() {
  Promise.resolve(
    ipcRenderer.sendSync('getEnd', this.state.inventario_id)
  ).then((enderecamento)=>{
    console.log('executou o LerEnd');
    this.setState({enderecamento : enderecamento.sort((a,b)=>{
      return a.descricao.split('-')[1] - b.descricao.split('-')[1]
    })}, () => {
      this.atualizarGraficoPizza();
    })
  })
}
lerEquipe() {
  Promise.resolve(
    ipcRenderer.sendSync('getEquipe', this.state.inventario_id)
  ).then( equipe => {
    let qtd_enderecamentoTotal = 0
    let qtd_concluidoTotal = 0
    
    const eq = equipe.map(i => {
      i['progress'] = Number(((i['qtd_concluido'] / i['qtd_enderecamento'])*100).toFixed(1))
      qtd_enderecamentoTotal += i.qtd_enderecamento
      qtd_concluidoTotal += i.qtd_concluido
      return i
    })

    const progressTotal = Number(((qtd_concluidoTotal / qtd_enderecamentoTotal)*100).toFixed(1))
    return { equipe: eq, progressTotal }
  }).then(({ equipe, progressTotal }) => {
    console.log('Edu:', { equipe, progressTotal })
    this.setState({ equipe, progressTotal })
  })
}
segundaContagem(){
  alert('segunda contagem');
}
liberarValidacao(){
  Promise.resolve(
    ipcRenderer.sendSync('destivarValidacao')
  ).then(retorno => {
    this.setState({validacao: retorno}, ()=> {
      alert('Status de Validação: '+retorno);
    });
  })
}

render() {

  const options = {
    defaultSortName: 'valor_divergente', 
    noDataText: 'Não há dados para exibir',
    exportCSVText: 'Exportar para csv'
  }
  localStorage.setItem('isDivergenciaPorGrupo', false);
  const { alertas,base, coleta, timeFormat, isPaused, enderecamento, equipe, progressTotal, validacao, data_grafico_pizza } = this.state

  return (
    <div className="content">
      <h1>Dashboard</h1>
      <Container fluid>
        <Row>
          <Col
          >
            <Card
              // title="ENDERECAMENTOS"
              content={
                <div style={{
                  overflow: 'auto',
                  height: '380px'
                }}>
                  
                  <Cardlist 
                    // lista={enderecamento}
                    coleta={coleta}
                    lista={enderecamento}
                  />
                  <Button variant="info" size="sm" >
                    Validação: {validacao}
                  </Button>
                </div>
              }>
            </Card>
          </Col>
          <Col md={4}>
            <Card 
              title = "Endereçamentos"
              content = {
                <div style={{height: '350px'}}>
                  <Cardpizza data_grafico_pizza={data_grafico_pizza}/>
                </div>
              }
            >
            </Card>
          </Col>
          <Col md={2}>
            <Card
              content={
                <div style={{
                  overflow: 'none',
                  height: '380px'
                }}>
                  <div className=" row d-flex justify-content-around"><h6 style={{'font-size':'20px'}}>{timeFormat}</h6></div>
                  <div className=" btns-clock justify-content-around mb-3">
                    <div className="btn-clock" size="sm" disabled={isPaused?false:true} onClick={this.playClock}>Play</div>
                    <div className="btn-clock" size="sm" disabled={isPaused?true:false} onClick={this.pauseClock}>||</div>
                    <div className="btn-clock" size="sm" onClick={this.segundaContagem}>[=]</div>
                    <div className="btn-clock" size="sm" onClick={this.stopClock}>V</div>
                    <div className="btn-clock" style={{'background':'tomato'}} size="sm" onClick={e => this.liberarValidacao()}>F</div>
                  </div>
                </div>
              }
            />
          </Col>
        </Row>
        <Row>
        <Col md={4} style={{
                'marginBottom': '30px'
            }}>
          <Card 
            title = "Coleta"
            content = {
              <BootstrapTable pagination data={coleta} height='250' scrollTop={ 'Bottom' } 
                search exportCSV options={ {
                defaultSortName: 'valor_divergente', 
                noDataText: 'Não há dados para exibir',
                exportCSVText: 'Exportar para csv',
                defaultSortName: 'qtd',  // default sort column name
                defaultSortOrder: 'desc'  // default sort order
              } }>
                  <TableHeaderColumn dataField='id' isKey hidden>ID</TableHeaderColumn>
                  <TableHeaderColumn dataField='cod_barra' width='140' >EAN COLETA ({coleta.length})</TableHeaderColumn>
                  {/* <TableHeaderColumn dataField='enderecamento'>Enderecamento</TableHeaderColumn> */}
                  <TableHeaderColumn dataField='qtd' width='70'>QUANT</TableHeaderColumn>
                </BootstrapTable>
            }
          />
          </Col>
          <Col md={4} style={{
                'marginBottom': '30px'
            }}>
            <Card 
              title = "Codigos na Base"
              content = {
                <BootstrapTable
                  
                  pagination data={base} height='250' scrollTop={ 'Bottom' } 
                  search options={ options }>
                  <TableHeaderColumn dataField='_id' isKey hidden>ID</TableHeaderColumn>
                  <TableHeaderColumn dataField='cod_barras' width='140' >EAN BASE ({base.length})</TableHeaderColumn>
                  {/* <TableHeaderColumn dataField='descricao_item' tdStyle={{whiteSpace: 'normal'}}>DESCRIÇÃO</TableHeaderColumn> */}
                  <TableHeaderColumn dataField='saldo_qtd_estoque' width='70'>SALDO</TableHeaderColumn>
                </BootstrapTable>
              }
            />
          </Col>
          <Col md={4}>
          <Card
              title="Alertas de Funcionários"
              content={
                <div style={{
                  overflow: 'none',
                  height: '370px'
                }}>
                  <Alertaslist alertas = {alertas.alertas} />
                </div>
              }
            />
          </Col>
        </Row>

      </Container>
    </div>
  );
}
}

export default Dashboard;
